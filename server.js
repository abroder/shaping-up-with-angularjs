var http = require('http');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var User = require('./app/models/user');

var api = require('./app/api');
var config = require('./app/config');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
	resave: false,
	saveUninitialized: false,
	secret: 'Hello, world!'
}));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy({
		usernameField: 'username',
		passwordField: 'password'
	}, function (username, password, done) {
		User.findOne({username: username}, function (err, user) {
			if (err) return done(err);
			else if (!user) return done(null, false, {message: "The user does not exist"});
			else if (user.password !== password) return done(null, false, {message: "Wrong password"});
			else return done(null, user);
		});
	}
));

passport.serializeUser(function (user, done) {
	done(null, user['_id']);
});

passport.deserializeUser(function (id, done) {
	User.findOne({_id: id}, function (err, user) {
		if (err) return done(new Error('User ' + id + ' does not exist.'));
		done(null, user);
	});
});

app.use('/api', api);
app.use(express.static(__dirname + '/public'));

app.get('*', function (req, res) {
	res.sendFile('public/index.html', {
		root: __dirname,
	});
});

mongoose.connect(config.db);
var server = app.listen(3000);
server.on('close', function() {
	mongoose.disconnect();
});