var express = require('express'),
   mongoose = require('mongoose'),
     multer = require('multer'),
          _ = require('underscore'),
   passport = require('passport'),
        Gem = require('./models/gem.js');

var router = express.Router();

router.get('/gems', function (req, res) {
	Gem.find(function (err, data) {
		if (err) return console.log(err);
		res.json(data);
		res.end();
	});
});

var auth = function(req, res, next) {
	if (!req.isAuthenticated()) {
		res.sendStatus(401);
	} else {
		next();
	}
}

router.post('/gems', auth, [multer({dest: './public/uploads', rename: function (fieldname, filename) { return filename + Date.now(); }}), function (req, res) {
	var newGem = req.body;
	newGem.images = _.map(req.files, function(file) {
		return file['path'].substring("public".length); 
	})

	Gem.create(newGem, function (err, small) {
		if (err) {
			res.writeHead(500);
			res.end();
			return console.log(err);
		}

		res.writeHead(200);
		res.end();
	});
}]);

router.post('/gems/:id/review',
	function (req, res) {
		Gem.find({_id: req.params.id}, function (err, data) {
			if (err) return (console.log(err));
			var gem = data[0];
			gem['reviews'].push(req.body);
			gem.save(function (err) {
				if (err) return console.log(err);
			});
		});
	}
);

router.post('/login', passport.authenticate('local'), function (req, res, next) {
	res.send(req.user);
});

module.exports = router;
