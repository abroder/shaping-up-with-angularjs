var mongoose = require('mongoose');

module.exports = mongoose.model('Gem', {
	name: String,
	price: Number,
	description: String,
	canPurchase: Boolean,
	soldOut: Boolean,
	images: [String],
	reviews: [{
		stars: Number,
		body: String,
		author: String
	}]
});