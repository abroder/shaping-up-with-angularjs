(function() {
	var app = angular.module('store-products', []);

	app.directive('productTitle', function() {
		return {
			restrict: 'E',
			templateUrl: 'views/product-title.html',
		};
	});

	app.directive('productPanels', function() {
		return {
			restrict: 'E',
			templateUrl: 'views/product-panels.html',
			controller: function() {
				this.tab = 1;

				this.selectTab = function(setTab) {
					this.tab = setTab;
				};

				this.isSelected = function(checkTab) {
					return this.tab === checkTab;
				};
			},
			controllerAs: "panel",
		}
	});

	app.controller("ReviewController", ['$http', function($http) {
		this.review = {};

		this.addReview = function(product) {
			product.reviews.push(this.review);
			
			var formData = {
				stars: this.review.stars,
				body: this.review.body,
				author: this.review.author
			};

			this.review = {};

			$http.post('/api/gems/' + product['_id'] + '/review', formData)
				.success(function(data, status, headers, config) {
					console.log("success");
				});

		};
	}]);

})();