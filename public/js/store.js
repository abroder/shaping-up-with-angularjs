(function() {
	var app = angular.module('store', ['store-products']);

	app.controller('StoreController', ['$http', function($http) {
		var store = this;
		$http.get('/api/gems').success(function(data) {
			store.products = data;
		});
	}]);
})();