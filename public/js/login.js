(function() {
	var app = angular.module('login', []);

	app.controller('LoginController', ['$http', '$window', '$scope', function($http, $window, $scope) {
		this.user = {};

		this.login = function() {			
			$http.post('/api/login', {username: this.user.username, password: this.user.password}).
				success(function(data, status, headers, config) {
					console.log(data);
					if (data.success) {
						$window.location = '/';
					} else {
						
					}
				});
		}
	}]);


})();