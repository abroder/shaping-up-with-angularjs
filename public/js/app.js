(function() {
	var app = angular.module('gemStoreApp', [
			'store',
			'admin',
			'login',
			'ngRoute'
		]);

	app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
		$routeProvider.
		when('/', {
			templateUrl: 'views/pages/store.html',
			controller: 'StoreController'
		}).
		when('/admin', {
			templateUrl: 'views/pages/admin.html',
			controller: 'AdminController'
		}).
		when('/login', {
			templateUrl: 'views/pages/login.html',
			controller: 'LoginController'
		}).
		otherwise({
			templateUrl: 'views/pages/404.html'
		});

		$locationProvider.html5Mode(true);
	}]);

})();