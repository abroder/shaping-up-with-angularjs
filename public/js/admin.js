(function() {
	var app = angular.module('admin', ['angularFileUpload']);

	app.controller('AdminController', ['$upload', '$window', '$scope', function($upload, $window, $scope) {
		this.gem = {
			canPurchase: true
		};

		this.submitGem = function() {
			$scope.upload = $upload.upload({
				url: '/api/gems',
				data: this.gem,
				file: $scope.files
			}).success(function(data, status, headers, config) {
				$window.location = '/';
			}).error(function(data, status, headers, config) {
				console.log(data);
				// $window.location = '/login';
			});
		};
	}]);
})();