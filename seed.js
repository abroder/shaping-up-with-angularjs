(function() {
    var mongoose = require('mongoose');
    var _ = require('underscore');

    var Gem = require('./app/models/gem.js');
    var User = require('./app/models/user.js');

    var seedData = [
    {
        "name": "Dodecahedron",
        "price": 2.95,
        "description": "Some gems have hidden qualities beyond their luster, beyond their shine... Dodeca is one of those gems.",
        "canPurchase": true,
        "soldOut": false,
        "images": [
        "img/gem-01.gif"
        ],
        "reviews": []
    },
    {
        "name": "Pentagonal Gem",
        "price": 5.95,
        "description": "Origin of the Pentagonal Gem is unknown, hence its low value. It has a very high shine and 12 sides, however.",
        "canPurchase": false,
        "soldOut": false,
        "images": [
        "img/gem-02.gif"
        ],
        "reviews": []
    }
    ];
    var saveCount = 0;

    mongoose.connect('mongodb://localhost/shaping-up');

    mongoose.connection.once('open', function () {
        _.each(seedData, function (item) {
            var promise = Gem.create(item, function (err) {
                if (err) return console.error(err);
            });

            promise.then(function () {
                saveCount++;
                if (saveCount >= seedData.length) {
                    mongoose.disconnect();
                }
            });
        });

        User.create({
            username: 'admin',
            password: 'admin'
        }, function (err) {
            if (err) return console.error(err);
        });
    });
})();